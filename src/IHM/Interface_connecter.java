package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class Interface_connecter {

	private JFrame frame;
	private JTextField textField;
	private JPasswordField passwordField;
	private JLabel lblVeillezVousAuthentifiez;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_connecter window = new Interface_connecter();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_connecter() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 572, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		textField.setColumns(10);
		textField.setBounds(161, 91, 176, 27);
		frame.getContentPane().add(textField);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		passwordField.setBounds(161, 171, 176, 27);
		frame.getContentPane().add(passwordField);
		
		JLabel lblNewLabel = new JLabel("Nom\r\n");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblNewLabel.setBounds(33, 93, 68, 21);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Mot de passe");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(33, 167, 111, 33);
		frame.getContentPane().add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Se connecter");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				
				String name= textField.getText();
				String passe= passwordField.getText();

				if (name.equals("qasim")&&passe.equals("qasim")) {
					//JOptionPane.showMessageDialog(frame, "connection reussie !!!");
				
					Interface_menu windows = new Interface_menu();
						windows.getFrame().setVisible(true);
					
				}
					else {
					JOptionPane.showMessageDialog(frame, "Nom ou Mot de passe invalide, reessayer!!!");
					
				}
			
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(216, 248, 121, 27);
		frame.getContentPane().add(btnNewButton);
		
		lblVeillezVousAuthentifiez = new JLabel("Veillez vous authentifiez");
		lblVeillezVousAuthentifiez.setForeground(Color.BLACK);
		lblVeillezVousAuthentifiez.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblVeillezVousAuthentifiez.setBackground(Color.CYAN);
		lblVeillezVousAuthentifiez.setBounds(118, 10, 388, 52);
		frame.getContentPane().add(lblVeillezVousAuthentifiez);
		
		lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon(Interface_connecter.class.getResource("/Images/pass.png")));
		lblNewLabel_2.setBounds(401, 59, 191, 195);
		frame.getContentPane().add(lblNewLabel_2);
	}
}
