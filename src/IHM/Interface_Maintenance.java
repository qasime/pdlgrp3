package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import javax.swing.JTextField;

import dao.ClientDAO;
import model.Client;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Interface_Maintenance extends JFrame{
	private JTextField Id;
	private JTextField Date;
	private JTextField NomEnt;
	private boolean maintenance;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_Maintenance window = new Interface_Maintenance();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_Maintenance() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		this.getContentPane().setForeground(Color.MAGENTA);
		this.getContentPane().setBackground(SystemColor.inactiveCaption);
		this.setBounds(100, 100, 589, 463);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBounds(34, 10, 166, 42);
		this.getContentPane().add(lblGmao);
		
		JLabel lblIdmaintenance = new JLabel("id_Maintenance\r\n");
		lblIdmaintenance.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdmaintenance.setBounds(36, 150, 166, 21);
		this.getContentPane().add(lblIdmaintenance);
		
		JLabel lblTypemaintenance = new JLabel("Type_Maintenance");
		lblTypemaintenance.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblTypemaintenance.setBounds(30, 205, 170, 21);
		this.getContentPane().add(lblTypemaintenance);
		
		JLabel lblDatedemande = new JLabel("Date_Demande");
		lblDatedemande.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblDatedemande.setBounds(34, 258, 149, 21);
		this.getContentPane().add(lblDatedemande);
		
		JLabel lblNomequipement = new JLabel("Nom_Equipement");
		lblNomequipement.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNomequipement.setBounds(34, 314, 149, 21);
		this.getContentPane().add(lblNomequipement);
		
		JButton btnValider_1 = new JButton("Valider");
		btnValider_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!maintenance) {
					Id.setText("");
					Date.setText("");
					NomEnt.setText("");
					
					maintenance = true;
				}else {
					new MaintenanceDAO().add(new Maintenance(Integer.parseInt(Id.getText()), Date.getText(), NomEnt.getText()));
					Id.setEditable(false);
					maintenance = false;
					//tree.setModel(new DefaultTreeModel(findNextTreeNode("Stocks")));
			}
			}
		});
		btnValider_1.setForeground(Color.MAGENTA);
		btnValider_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnValider_1.setBounds(234, 367, 121, 27);
		this.getContentPane().add(btnValider_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(0, 65, 626, 10);
		this.getContentPane().add(panel);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(240, 4, 325, 52);
		this.getContentPane().add(lblBienvenuSurNotre);
		
		JLabel lblDemandeDeMaintenance = new JLabel("Demande de maintenance");
		lblDemandeDeMaintenance.setForeground(Color.MAGENTA);
		lblDemandeDeMaintenance.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblDemandeDeMaintenance.setBackground(Color.WHITE);
		lblDemandeDeMaintenance.setBounds(178, 93, 251, 21);
		this.getContentPane().add(lblDemandeDeMaintenance);
		
		Id = new JTextField();
		Id.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Id.setColumns(10);
		Id.setBounds(223, 144, 306, 27);
		getContentPane().add(Id);
		
		Date = new JTextField();
		Date.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Date.setColumns(10);
		Date.setBounds(223, 252, 306, 27);
		getContentPane().add(Date);
		
		NomEnt = new JTextField();
		NomEnt.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		NomEnt.setColumns(10);
		NomEnt.setBounds(223, 308, 306, 27);
		getContentPane().add(NomEnt);
		
		JComboBox Type = new JComboBox();
		Type.setFont(new Font("Times New Roman", Font.BOLD, 16));
		Type.setModel(new DefaultComboBoxModel(new String[] {"Préventive", "Curative", "Palliative"}));
		Type.setBounds(223, 202, 306, 27);
		getContentPane().add(Type);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menu_maintenance windows = new menu_maintenance();
				windows.setVisible(true);
			}
		});
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(34, 368, 121, 27);
		getContentPane().add(btnValider_1_1);
	}
 	void initForms(){
 		Id.setText("");
		Date.setText("");
		NomEnt.setText("");
}}
