package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JTextField;

import dao.ClientDAO;
import model.Client;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interface_devis extends JFrame{

	private JFrame frame;
	private JTextField Id;
	private JTextField designation;
	private JTextField Quantité;
	private JTextField PU;
	private JTextField PT;
	private boolean devis;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_devis window = new Interface_devis();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_devis() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//frame = new JFrame();
		this.getContentPane().setBackground(SystemColor.inactiveCaption);
		this.setBounds(100, 100, 609, 568);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBounds(14, 10, 197, 42);
		this.getContentPane().add(lblGmao);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(0, 72, 595, 10);
		this.getContentPane().add(panel);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(223, 10, 372, 52);
		this.getContentPane().add(lblBienvenuSurNotre);
		
		JLabel lblCreerUnDevis = new JLabel("Creer un devis");
		lblCreerUnDevis.setForeground(Color.MAGENTA);
		lblCreerUnDevis.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblCreerUnDevis.setBackground(Color.WHITE);
		lblCreerUnDevis.setBounds(210, 103, 220, 21);
		this.getContentPane().add(lblCreerUnDevis);
		
		JLabel lblIdclient = new JLabel("id_Devis\r\n");
		lblIdclient.setBackground(new Color(240, 240, 240));
		lblIdclient.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdclient.setBounds(48, 167, 89, 21);
		this.getContentPane().add(lblIdclient);
		
		JLabel lblDsignation = new JLabel("Désignation");
		lblDsignation.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblDsignation.setBounds(48, 224, 126, 21);
		this.getContentPane().add(lblDsignation);
		
		JLabel lblQuantit = new JLabel("Quantité");
		lblQuantit.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblQuantit.setBounds(48, 292, 89, 21);
		this.getContentPane().add(lblQuantit);
		
		JLabel lblPrixUnitaire = new JLabel("Prix unitaire");
		lblPrixUnitaire.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblPrixUnitaire.setBounds(48, 355, 126, 21);
		this.getContentPane().add(lblPrixUnitaire);
		
		JLabel lblPrixTotal = new JLabel("Prix total");
		lblPrixTotal.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblPrixTotal.setBounds(48, 420, 89, 21);
		this.getContentPane().add(lblPrixTotal);
		
		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!devis) {
					Id.setText("");
					designation.setText("");
					Quantité.setText("");
					PU.setText("");
					PT.setText("");
					devis = true;
				}else {
					new DevisDAO.add(new Devis(Integer.parseInt(Id.getText()), Id.getText(), designation.getText(), Quantité.getText(), PU.getText(), PT.getText()));
					Id.setEditable(false);
					devis = false;
					//tree.setModel(new DefaultTreeModel(findNextTreeNode("Stocks")));
			}
			}
			
		});
		btnValider.setForeground(Color.MAGENTA);
		btnValider.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnValider.setBounds(230, 475, 121, 27);
		this.getContentPane().add(btnValider);
		
		Id = new JTextField();
		Id.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Id.setColumns(10);
		Id.setBounds(220, 161, 306, 27);
		getContentPane().add(Id);
		
		designation = new JTextField();
		designation.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		designation.setColumns(10);
		designation.setBounds(220, 218, 306, 27);
		getContentPane().add(designation);
		
		Quantité = new JTextField();
		Quantité.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Quantité.setColumns(10);
		Quantité.setBounds(220, 286, 306, 27);
		getContentPane().add(Quantité);
		
		PU = new JTextField();
		PU.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		PU.setColumns(10);
		PU.setBounds(220, 353, 306, 27);
		getContentPane().add(PU);
		
		PT = new JTextField();
		PT.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		PT.setColumns(10);
		PT.setBounds(220, 414, 306, 27);
		getContentPane().add(PT);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(35, 475, 121, 27);
		getContentPane().add(btnValider_1_1);
	}
	void initForms()
	{
		Id.setText("");
		designation.setText("");
		Quantité.setText("");
		PU.setText("");
		PT.setText("");
		devis = true;
	}
}
