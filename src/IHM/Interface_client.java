package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.tree.DefaultTreeModel;

import dao.ClientDAO;

import model.Client;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interface_client extends JFrame{
	private JTextField Id;
	private JTextField Nom;
	private JTextField NumeroIFU;
	private JTextField NumeroRCCM;
	private JTextField Adresse;
	private JTextField CodeAPE;
	private boolean client;

	//private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_client window = new Interface_client();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_client() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		client = false;
		getContentPane().setBackground(SystemColor.inactiveCaption);
		setBounds(100, 100, 594, 548);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblIdclient = new JLabel("id_Client\r\n");
		lblIdclient.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdclient.setBounds(35, 144, 89, 21);
		getContentPane().add(lblIdclient);
		
		JLabel lblNomentreprise = new JLabel("Nom_Entreprise");
		lblNomentreprise.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNomentreprise.setBounds(35, 197, 144, 21);
		getContentPane().add(lblNomentreprise);
		
		JLabel lblNumeroifu = new JLabel("Numero_IFU");
		lblNumeroifu.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNumeroifu.setBounds(35, 249, 125, 21);
		getContentPane().add(lblNumeroifu);
		
		JLabel lblNumerorccm = new JLabel("Numero_RCCM");
		lblNumerorccm.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNumerorccm.setBounds(35, 301, 181, 21);
		getContentPane().add(lblNumerorccm);
		
		JLabel lblAdresseentreprise = new JLabel("Adresse_Entreprise");
		lblAdresseentreprise.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblAdresseentreprise.setBounds(35, 350, 181, 21);
		getContentPane().add(lblAdresseentreprise);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBounds(24, 10, 166, 42);
		getContentPane().add(lblGmao);
		
		JLabel lblNewLabel_4_1 = new JLabel("Code_APE");
		lblNewLabel_4_1.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNewLabel_4_1.setBounds(35, 404, 144, 21);
		getContentPane().add(lblNewLabel_4_1);
		
		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!client) {
					Id.setText("");
					Nom.setText("");
					NumeroIFU.setText("");
					NumeroRCCM.setText("");
					Adresse.setText("");
					CodeAPE.setText("");
					client = true;
				}else {
					new ClientDAO().add(new Client(Integer.parseInt(Id.getText()), Nom.getText(), NumeroIFU.getText(), NumeroRCCM.getText(), NumeroIFU.getText(), Adresse.getText()));
					Id.setEditable(false);
					client = false;
					//tree.setModel(new DefaultTreeModel(findNextTreeNode("Stocks")));
			}
		}});
		btnValider.setForeground(Color.MAGENTA);
		btnValider.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnValider.setBounds(226, 461, 121, 27);
		getContentPane().add(btnValider);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(233, 10, 335, 52);
		getContentPane().add(lblBienvenuSurNotre);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(10, 72, 546, 10);
		getContentPane().add(panel);
		
		JLabel lblSaisirUnClient = new JLabel("Saisir un Client");
		lblSaisirUnClient.setForeground(Color.MAGENTA);
		lblSaisirUnClient.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblSaisirUnClient.setBackground(Color.WHITE);
		lblSaisirUnClient.setBounds(204, 92, 160, 21);
		getContentPane().add(lblSaisirUnClient);
		
		Id = new JTextField();
		Id.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Id.setColumns(10);
		Id.setBounds(229, 138, 306, 27);
		getContentPane().add(Id);
		
		Nom = new JTextField();
		Nom.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Nom.setColumns(10);
		Nom.setBounds(229, 191, 306, 27);
		getContentPane().add(Nom);
		
		NumeroIFU = new JTextField();
		NumeroIFU.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		NumeroIFU.setColumns(10);
		NumeroIFU.setBounds(229, 243, 306, 27);
		getContentPane().add(NumeroIFU);
		
		NumeroRCCM = new JTextField();
		NumeroRCCM.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		NumeroRCCM.setColumns(10);
		NumeroRCCM.setBounds(229, 295, 306, 27);
		getContentPane().add(NumeroRCCM);
		
		Adresse = new JTextField();
		Adresse.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Adresse.setColumns(10);
		Adresse.setBounds(229, 344, 306, 27);
		getContentPane().add(Adresse);
		
		CodeAPE = new JTextField();
		CodeAPE.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		CodeAPE.setColumns(10);
		CodeAPE.setBounds(229, 398, 306, 27);
		getContentPane().add(CodeAPE);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(24, 462, 121, 27);
		getContentPane().add(btnValider_1_1);
	}

void initForms(){
	Id.setText("");
	Nom.setText("");
	NumeroIFU.setText("");
	NumeroRCCM.setText("");
	Adresse.setText("");
	CodeAPE.setText("");
	client = true;
}}
