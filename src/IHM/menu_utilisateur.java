package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ImageIcon;

public class menu_utilisateur extends JFrame{

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					menu_utilisateur window = new menu_utilisateur();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public menu_utilisateur() {
		getContentPane().setBackground(new Color(0, 255, 255));
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
	    this.setBounds(100, 100, 642, 474);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.getContentPane().setLayout(null);
		
		JButton btnCreerUnClient = new JButton("Creer un client");
		btnCreerUnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Interface_client windows = new Interface_client();
				windows.setVisible(true);
			}
		});
		btnCreerUnClient.setForeground(Color.BLACK);
		btnCreerUnClient.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnCreerUnClient.setBackground(Color.ORANGE);
		btnCreerUnClient.setBounds(60, 146, 214, 33);
		this.getContentPane().add(btnCreerUnClient);
		
		JButton btnSaisirUnOprateur = new JButton("Saisir un opérateur");
		btnSaisirUnOprateur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Interface_opérateur windows = new Interface_opérateur();
				windows.setVisible(true);
			}
		});
		btnSaisirUnOprateur.setForeground(Color.BLACK);
		btnSaisirUnOprateur.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnSaisirUnOprateur.setBackground(Color.ORANGE);
		btnSaisirUnOprateur.setBounds(60, 243, 214, 33);
		this.getContentPane().add(btnSaisirUnOprateur);
		
		JButton btnAffecterOprateur = new JButton("Affecter un opérateur");
		btnAffecterOprateur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Interface_affectation windows = new Interface_affectation();
				windows.setVisible(true);
			}
		});
		btnAffecterOprateur.setForeground(Color.BLACK);
		btnAffecterOprateur.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnAffecterOprateur.setBackground(Color.ORANGE);
		btnAffecterOprateur.setBounds(60, 345, 214, 33);
		getContentPane().add(btnAffecterOprateur);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 36));
		lblGmao.setBackground(Color.CYAN);
		lblGmao.setBounds(25, 10, 209, 52);
		getContentPane().add(lblGmao);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(287, 10, 345, 52);
		getContentPane().add(lblBienvenuSurNotre);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(-1, 66, 633, 10);
		getContentPane().add(panel);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(menu_utilisateur.class.getResource("/Images/im1.png")));
		lblNewLabel.setBounds(-1, 0, 628, 437);
		getContentPane().add(lblNewLabel);
	}
	

}
