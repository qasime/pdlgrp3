package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class menu_gestion extends JFrame{

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					menu_gestion window = new menu_gestion();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public menu_gestion() {
		getContentPane().setBackground(Color.CYAN);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		this.setBounds(100, 100, 667, 485);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 36));
		lblGmao.setBackground(Color.CYAN);
		lblGmao.setBounds(10, 10, 209, 52);
		this.getContentPane().add(lblGmao);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(266, 10, 345, 52);
		this.getContentPane().add(lblBienvenuSurNotre);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(10, 68, 633, 10);
		this.getContentPane().add(panel);
		
		JButton btnCreerUnClient = new JButton("Reporting");
		btnCreerUnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Interface_Reporting windows = new Interface_Reporting();
				windows.setVisible(true);
			}
		});
		btnCreerUnClient.setForeground(Color.RED);
		btnCreerUnClient.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnCreerUnClient.setBackground(Color.ORANGE);
		btnCreerUnClient.setBounds(58, 170, 214, 33);
		this.getContentPane().add(btnCreerUnClient);
		
		JButton btnSaisirUnOprateur = new JButton("Devis");
		btnSaisirUnOprateur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Interface_devis windows = new Interface_devis();
				windows.setVisible(true);
			}
		});
		btnSaisirUnOprateur.setForeground(Color.RED);
		btnSaisirUnOprateur.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnSaisirUnOprateur.setBackground(Color.ORANGE);
		btnSaisirUnOprateur.setBounds(58, 286, 214, 33);
		this.getContentPane().add(btnSaisirUnOprateur);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(menu_gestion.class.getResource("/Images/im1.png")));
		lblNewLabel.setBounds(0, 0, 653, 448);
		getContentPane().add(lblNewLabel);
	}
}
