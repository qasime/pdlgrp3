package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import dao.ClientDAO;
import model.Client;

public class Interface_Fiche_Maintenance extends JFrame{

	private JFrame frame;
	private JTextField IdFiche;
	private JTextField IdOperateur;
	private JTextField Type;
	private JTextField Duree;
	private boolean fiche;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_Fiche_Maintenance window = new Interface_Fiche_Maintenance();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_Fiche_Maintenance() {
		getContentPane().setFont(new Font("Times New Roman", Font.PLAIN, 16));
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//frame = new JFrame();
		this.getContentPane().setBackground(SystemColor.inactiveCaption);
		this.setBounds(100, 100, 611, 593);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBounds(10, 10, 166, 42);
		this.getContentPane().add(lblGmao);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(258, 4, 329, 52);
		this.getContentPane().add(lblBienvenuSurNotre);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(10, 56, 577, 10);
		this.getContentPane().add(panel);
		
		JLabel lblIdfichemaintenance = new JLabel("id_Fiche_Maintenance\r\n");
		lblIdfichemaintenance.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdfichemaintenance.setBounds(39, 133, 189, 21);
		this.getContentPane().add(lblIdfichemaintenance);
		
		JLabel lblTypepanne = new JLabel("Type_Panne");
		lblTypepanne.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblTypepanne.setBounds(39, 242, 144, 21);
		this.getContentPane().add(lblTypepanne);
		
		JLabel lblDureintervention = new JLabel("Durée_Intervention\r\n");
		lblDureintervention.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblDureintervention.setBounds(39, 300, 189, 21);
		this.getContentPane().add(lblDureintervention);
		
		JLabel lblCommentaire = new JLabel("Commentaire");
		lblCommentaire.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblCommentaire.setBounds(39, 399, 144, 21);
		this.getContentPane().add(lblCommentaire);
		
		JButton btnValider_1 = new JButton("Valider");
		btnValider_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fiche) {
					IdFiche.setText("");
					IdOperateur.setText("");
					Type.setText("");
					Duree.setText("");
					fiche = true;
				}else {
					new FicheDAO().add(new Fiche(Integer.parseInt(IdFiche.getText()), IdFiche.getText(), IdOperateur.getText(), Type.getText(), Duree.getText(), Adresse.getText()));
					IdFiche.setEditable(false);
					fiche = false;
					//tree.setModel(new DefaultTreeModel(findNextTreeNode("Stocks")));
			}
			}
		});
		btnValider_1.setForeground(Color.MAGENTA);
		btnValider_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnValider_1.setBounds(246, 497, 121, 27);
		this.getContentPane().add(btnValider_1);
		
		JLabel lblRemplirLaFiche = new JLabel("Remplir la fiche");
		lblRemplirLaFiche.setForeground(Color.MAGENTA);
		lblRemplirLaFiche.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblRemplirLaFiche.setBackground(Color.WHITE);
		lblRemplirLaFiche.setBounds(188, 82, 156, 21);
		this.getContentPane().add(lblRemplirLaFiche);
		
		JLabel lblIdoperateur = new JLabel("id_Operateur");
		lblIdoperateur.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdoperateur.setBounds(39, 186, 189, 21);
		getContentPane().add(lblIdoperateur);
		
		IdFiche = new JTextField();
		IdFiche.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		IdFiche.setColumns(10);
		IdFiche.setBounds(246, 131, 306, 27);
		getContentPane().add(IdFiche);
		
		IdOperateur = new JTextField();
		IdOperateur.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		IdOperateur.setColumns(10);
		IdOperateur.setBounds(246, 180, 306, 27);
		getContentPane().add(IdOperateur);
		
		Type = new JTextField();
		Type.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Type.setColumns(10);
		Type.setBounds(249, 236, 306, 27);
		getContentPane().add(Type);
		
		Duree = new JTextField();
		Duree.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Duree.setColumns(10);
		Duree.setBounds(249, 294, 306, 27);
		getContentPane().add(Duree);
		
		JTextPane Commentaire = new JTextPane();
		Commentaire.setBounds(246, 358, 306, 97);
		getContentPane().add(Commentaire);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menu_maintenance windows = new menu_maintenance();
				windows.setVisible(true);
			}
		});
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(39, 497, 121, 27);
		getContentPane().add(btnValider_1_1);
	}

void initForms(){
	IdFiche.setText("");
	IdOperateur.setText("");
	Type.setText("");
	Duree.setText("");
	fiche = true;

}}

