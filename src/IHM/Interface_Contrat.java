package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import javax.swing.JTextField;

import dao.ClientDAO;
import model.Client;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interface_Contrat extends JFrame{

	private JFrame frame;
	private JTextField IdContrat;
	private JTextField IdDemande;
	private JTextField NomEntClient;
	private JTextField NomEntResponsable;
	private JTextField Ouvrage;
	private JTextField Date;
	private JTextField Lieu;
	private boolean contrat;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_Contrat window = new Interface_Contrat();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_Contrat() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		this.getContentPane().setBackground(SystemColor.inactiveCaption);
		this.setBounds(100, 100, 655, 641);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblIdcontrat = new JLabel("id_Contrat\r\n");
		lblIdcontrat.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdcontrat.setBounds(45, 154, 144, 21);
		this.getContentPane().add(lblIdcontrat);
		
		JLabel lblNomentrepriseclient = new JLabel("Nom_Entreprise_Client");
		lblNomentrepriseclient.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNomentrepriseclient.setBounds(45, 263, 235, 21);
		this.getContentPane().add(lblNomentrepriseclient);
		
		JLabel lblNomentrepriseresponsable = new JLabel("Nom_Entreprise_Responsable");
		lblNomentrepriseresponsable.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNomentrepriseresponsable.setBounds(42, 317, 242, 21);
		this.getContentPane().add(lblNomentrepriseresponsable);
		
		JLabel lblOuvrageconcerne = new JLabel("Ouvrage_Concernée");
		lblOuvrageconcerne.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblOuvrageconcerne.setBounds(45, 374, 190, 21);
		this.getContentPane().add(lblOuvrageconcerne);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(10, 62, 621, 10);
		this.getContentPane().add(panel);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBounds(34, 10, 166, 42);
		this.getContentPane().add(lblGmao);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(290, 4, 329, 52);
		this.getContentPane().add(lblBienvenuSurNotre);
		
		JLabel lblOuvrageconcerne_1_1 = new JLabel("Date_Contrat");
		lblOuvrageconcerne_1_1.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblOuvrageconcerne_1_1.setBounds(45, 426, 144, 21);
		this.getContentPane().add(lblOuvrageconcerne_1_1);
		
		JLabel lblOuvrageconcerne_1_1_1 = new JLabel("Lieu\r\n");
		lblOuvrageconcerne_1_1_1.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblOuvrageconcerne_1_1_1.setBounds(56, 483, 144, 21);
		this.getContentPane().add(lblOuvrageconcerne_1_1_1);
		
		JButton btnValider_1 = new JButton("Valider");
		btnValider_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!contrat) {
					IdContrat.setText("");
					IdDemande.setText("");
					NomEntClient.setText("");
					NomEntResponsable.setText("");
					Ouvrage.setText("");
					Date.setText("");
					Lieu.setText("");
					contrat = true;
			}else {
				new ContratDAO.add(new Contrat(Integer.parseInt(IdContrat.getText()), IdDemande.getText(), NomEntClient.getText(), NomEntResponsable.getText(), Ouvrage.getText(), Date.getText(), Lieu.getText()));
				IdContrat.setEditable(false);
				contrat = false;
			}}});
		btnValider_1.setForeground(Color.MAGENTA);
		btnValider_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1.setBounds(251, 544, 121, 27);
		this.getContentPane().add(btnValider_1);
		
		JLabel lblValiderContrat = new JLabel("Valider Contrat");
		lblValiderContrat.setForeground(Color.MAGENTA);
		lblValiderContrat.setFont(new Font("Times New Roman", Font.PLAIN, 22));
		lblValiderContrat.setBackground(Color.WHITE);
		lblValiderContrat.setBounds(244, 92, 220, 21);
		this.getContentPane().add(lblValiderContrat);
		
		JLabel lblIddemandemaintenance = new JLabel("id_Demande_Maintenance\r\n");
		lblIddemandemaintenance.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIddemandemaintenance.setBounds(45, 205, 235, 21);
		getContentPane().add(lblIddemandemaintenance);
		
		IdContrat = new JTextField();
		IdContrat.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		IdContrat.setColumns(10);
		IdContrat.setBounds(293, 148, 306, 27);
		getContentPane().add(IdContrat);
		
		IdDemande = new JTextField();
		IdDemande.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		IdDemande.setColumns(10);
		IdDemande.setBounds(293, 199, 306, 27);
		getContentPane().add(IdDemande);
		
		NomEntClient = new JTextField();
		NomEntClient.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		NomEntClient.setColumns(10);
		NomEntClient.setBounds(293, 257, 306, 27);
		getContentPane().add(NomEntClient);
		
		NomEntResponsable = new JTextField();
		NomEntResponsable.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		NomEntResponsable.setColumns(10);
		NomEntResponsable.setBounds(293, 311, 306, 27);
		getContentPane().add(NomEntResponsable);
		
		Ouvrage = new JTextField();
		Ouvrage.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Ouvrage.setColumns(10);
		Ouvrage.setBounds(293, 368, 306, 27);
		getContentPane().add(Ouvrage);
		
		Date = new JTextField();
		Date.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Date.setColumns(10);
		Date.setBounds(293, 420, 306, 27);
		getContentPane().add(Date);
		
		Lieu = new JTextField();
		Lieu.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Lieu.setColumns(10);
		Lieu.setBounds(293, 477, 306, 27);
		getContentPane().add(Lieu);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(45, 544, 121, 27);
		getContentPane().add(btnValider_1_1);
	}
			void initForms()
		{
				IdContrat.setText("");
				IdDemande.setText("");
				NomEntClient.setText("");
				NomEntResponsable.setText("");
				Ouvrage.setText("");
				Date.setText("");
				Lieu.setText("");
				contrat = true;
		}
				
			
}
