package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class menu_maintenance extends JFrame{


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					menu_maintenance window = new menu_maintenance();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public menu_maintenance() {
		super();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//frame = new JFrame();
		getContentPane().setBackground(Color.CYAN);
		setBounds(100, 100, 683, 465);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnDemandeMaintenance = new JButton("Demande Maintenance");
		btnDemandeMaintenance.setBounds(33, 135, 233, 33);
		btnDemandeMaintenance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{

				Interface_Maintenance windows = new Interface_Maintenance();
				windows.setVisible(true);
			}
		});
		getContentPane().setLayout(null);
		btnDemandeMaintenance.setForeground(Color.BLACK);
		btnDemandeMaintenance.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnDemandeMaintenance.setBackground(Color.ORANGE);
		getContentPane().add(btnDemandeMaintenance);
		
		JButton btnFicheDeMaintenance = new JButton("Fiche de Maintenance");
		btnFicheDeMaintenance.setBounds(33, 239, 233, 33);
		btnFicheDeMaintenance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Interface_Fiche_Maintenance windows = new Interface_Fiche_Maintenance();
				windows.setVisible(true);
			}
		});
		btnFicheDeMaintenance.setForeground(Color.BLACK);
		btnFicheDeMaintenance.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnFicheDeMaintenance.setBackground(Color.ORANGE);
		getContentPane().add(btnFicheDeMaintenance);
		
		JButton btnValidationMaintenance = new JButton("Validation Maintenance");
		btnValidationMaintenance.setBounds(33, 351, 233, 33);
		btnValidationMaintenance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Interface_Validation windows = new Interface_Validation();
				windows.setVisible(true);
			}
		});
		btnValidationMaintenance.setForeground(Color.BLACK);
		btnValidationMaintenance.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnValidationMaintenance.setBackground(Color.ORANGE);
		getContentPane().add(btnValidationMaintenance);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setBounds(15, 10, 209, 52);
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 36));
		lblGmao.setBackground(Color.CYAN);
		getContentPane().add(lblGmao);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setBounds(264, 10, 345, 52);
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		getContentPane().add(lblBienvenuSurNotre);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 67, 669, 10);
		panel.setBackground(Color.MAGENTA);
		getContentPane().add(panel);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(menu_maintenance.class.getResource("/Images/im1.png")));
		lblNewLabel.setBounds(0, 0, 669, 428);
		getContentPane().add(lblNewLabel);
	}

}
