package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextPane;

import dao.ClientDAO;
import model.Client;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interface_Reporting extends JFrame{

	private JFrame frame;
	private JTextField id_Reporting;
	private JTextField id_Client;
	private JTextField id_Operateur;
	private boolean reporting;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_Reporting window = new Interface_Reporting();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_Reporting() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//frame = new JFrame();
		this.getContentPane().setBackground(SystemColor.inactiveCaption);
		this.setBounds(100, 100, 831, 625);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblIdreporting = new JLabel("id_Reporting");
		lblIdreporting.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdreporting.setBounds(38, 158, 144, 21);
		this.getContentPane().add(lblIdreporting);
		
		JLabel lblTypereporting = new JLabel("Type_Reporting");
		lblTypereporting.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblTypereporting.setBounds(550, 158, 144, 21);
		this.getContentPane().add(lblTypereporting);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBounds(25, 10, 166, 42);
		this.getContentPane().add(lblGmao);
		
		JButton btnValider_1 = new JButton("Valider");
		btnValider_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!reporting) {
					id_Reporting.setText("");
					id_Client.setText("");
					id_Operateur.setText("");
					reporting = true;
				}else {
					new ReportingDAO().add(new Reporting(Integer.parseInt(id_Reporting.getText()), id_Client.getText(), id_Operateur.getText() ));
					id_Reporting.setEditable(false);
					id_Client.setEditable(false);
					id_Operateur.setEditable(false);
					reporting = false;
					//tree.setModel(new DefaultTreeModel(findNextTreeNode("Stocks")));
			}
			}
		});
		btnValider_1.setForeground(Color.MAGENTA);
		btnValider_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1.setBounds(343, 538, 121, 27);
		this.getContentPane().add(btnValider_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(10, 55, 795, 10);
		this.getContentPane().add(panel);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(423, 4, 310, 52);
		this.getContentPane().add(lblBienvenuSurNotre);
		
		JLabel lblFaireUnReporting = new JLabel("Faire un Reporting");
		lblFaireUnReporting.setForeground(Color.MAGENTA);
		lblFaireUnReporting.setFont(new Font("Times New Roman", Font.PLAIN, 22));
		lblFaireUnReporting.setBackground(Color.WHITE);
		lblFaireUnReporting.setBounds(331, 77, 176, 21);
		this.getContentPane().add(lblFaireUnReporting);
		
		JLabel lblNomExecuteur = new JLabel("Nom_Exécuteur");
		lblNomExecuteur.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNomExecuteur.setBounds(36, 334, 144, 21);
		this.getContentPane().add(lblNomExecuteur);
		
		JLabel lblIdclient = new JLabel("id_Client");
		lblIdclient.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdclient.setBounds(36, 215, 144, 21);
		getContentPane().add(lblIdclient);
		
		JLabel lblIdoperateur = new JLabel("id_Operateur");
		lblIdoperateur.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdoperateur.setBounds(36, 273, 144, 21);
		getContentPane().add(lblIdoperateur);
		
		id_Reporting = new JTextField();
		id_Reporting.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		id_Reporting.setColumns(10);
		id_Reporting.setBounds(198, 152, 201, 27);
		getContentPane().add(id_Reporting);
		
		id_Client = new JTextField();
		id_Client.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		id_Client.setColumns(10);
		id_Client.setBounds(198, 209, 201, 27);
		getContentPane().add(id_Client);
		
		id_Operateur = new JTextField();
		id_Operateur.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		id_Operateur.setColumns(10);
		id_Operateur.setBounds(198, 267, 201, 27);
		getContentPane().add(id_Operateur);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Times New Roman", Font.BOLD, 16));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Client", "Opérateur"}));
		comboBox.setBounds(198, 329, 201, 29);
		getContentPane().add(comboBox);
		
		JTextPane TypeReporting = new JTextPane();
		TypeReporting.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		TypeReporting.setBounds(508, 192, 254, 273);
		getContentPane().add(TypeReporting);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(62, 539, 121, 27);
		getContentPane().add(btnValider_1_1);
	}

void initForms(){
	id_Reporting.setText("");
	id_Client.setText("");
	id_Operateur.setText("");
	reporting = true;
}}


