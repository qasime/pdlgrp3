package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

import dao.ClientDAO;
import model.Client;

public class Interface_Validation extends JFrame{

	private JFrame frame;
	private JTextField Id;
	private JTextField Debut;
	private JTextField Fin;
	private boolean validation;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_Validation window = new Interface_Validation();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_Validation() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		this.getContentPane().setBackground(SystemColor.inactiveCaption);
		this.setBounds(100, 100, 564, 438);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBounds(10, 10, 166, 42);
		this.getContentPane().add(lblGmao);
		
		JLabel lblIdreporting = new JLabel("id_Validation\r\n");
		lblIdreporting.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdreporting.setBounds(32, 166, 144, 21);
		this.getContentPane().add(lblIdreporting);
		
		JLabel lblIdreporting_1 = new JLabel("Date_Debut");
		lblIdreporting_1.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdreporting_1.setBounds(32, 226, 144, 21);
		this.getContentPane().add(lblIdreporting_1);
		
		JLabel lblIdreporting_2 = new JLabel("Date_Fin");
		lblIdreporting_2.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdreporting_2.setBounds(32, 292, 144, 21);
		this.getContentPane().add(lblIdreporting_2);
		
		JButton btnValider_1 = new JButton("Valider");
		btnValider_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!validation) {
					Id.setText("");
					Debut.setText("");
					Fin.setText("");
					validation = true;
				}else {
					new ValidationDAO().add(new Validation(Integer.parseInt(Id.getText()),Debut.getText(), Fin.getText()));
					Id.setEditable(false);
					validation = false;
					//tree.setModel(new DefaultTreeModel(findNextTreeNode("Stocks")));
			}
			}
		});
		btnValider_1.setForeground(Color.MAGENTA);
		btnValider_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnValider_1.setBounds(218, 349, 121, 27);
		this.getContentPane().add(btnValider_1);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(211, 4, 329, 52);
		this.getContentPane().add(lblBienvenuSurNotre);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(20, 62, 508, 10);
		this.getContentPane().add(panel);
		
		JLabel lblValiderMaintenance = new JLabel("Valider Maintenance");
		lblValiderMaintenance.setForeground(Color.MAGENTA);
		lblValiderMaintenance.setBackground(Color.WHITE);
		lblValiderMaintenance.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblValiderMaintenance.setBounds(172, 95, 208, 21);
		this.getContentPane().add(lblValiderMaintenance);
		
		Id = new JTextField();
		Id.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Id.setBounds(175, 161, 309, 27);
		getContentPane().add(Id);
		Id.setColumns(10);
		
		Debut = new JTextField();
		Debut.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Debut.setColumns(10);
		Debut.setBounds(175, 220, 309, 27);
		getContentPane().add(Debut);
		
		Fin = new JTextField();
		Fin.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Fin.setColumns(10);
		Fin.setBounds(175, 286, 309, 27);
		getContentPane().add(Fin);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				menu_maintenance windows = new menu_maintenance();
				windows.setVisible(true);
			}
		});
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(32, 349, 121, 27);
		getContentPane().add(btnValider_1_1);
	}

void initForms(){
	Id.setText("");
	Debut.setText("");
	Fin.setText("");
}}

