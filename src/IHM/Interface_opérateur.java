package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JTextField;

import dao.ClientDAO;
import model.Client;

import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interface_opérateur extends JFrame{

	private JFrame frame;
	private JTextField Id;
	private JTextField Nom;
	private JTextField Prenom;
	private JTextField Numero;
	private boolean operateur;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_opérateur window = new Interface_opérateur();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_opérateur() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		this.getContentPane().setBackground(SystemColor.inactiveCaption);
		this.setBounds(100, 100, 609, 464);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBackground(Color.CYAN);
		lblGmao.setBounds(27, 10, 166, 42);
		this.getContentPane().add(lblGmao);
		
		JLabel lblIdoprateur = new JLabel("id_Opérateur\r\n");
		lblIdoprateur.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdoprateur.setBounds(45, 141, 140, 21);
		this.getContentPane().add(lblIdoprateur);
		
		JLabel lblNom = new JLabel("Nom\r\n");
		lblNom.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNom.setBounds(45, 197, 89, 21);
		this.getContentPane().add(lblNom);
		
		JLabel lblPrnom = new JLabel("Prénom");
		lblPrnom.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblPrnom.setBounds(45, 255, 89, 21);
		this.getContentPane().add(lblPrnom);
		
		JLabel lblNumerotlphone = new JLabel("Numero_Téléphone");
		lblNumerotlphone.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNumerotlphone.setBounds(45, 311, 166, 21);
		this.getContentPane().add(lblNumerotlphone);
		
		JButton btnValider_1 = new JButton("Valider");
		btnValider_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!operateur) {
					Id.setText("");
					Nom.setText("");
					Prenom.setText("");
					Numero.setText("");
					operateur = true;
				}else {
					new OperateutDAO().add(new Operateu(Integer.parseInt(Id.getText()), Nom.getText(), Prenom.getText(), Numero.getText()));
					Id.setEditable(false);
					operateur = false;
					//tree.setModel(new DefaultTreeModel(findNextTreeNode("Stocks")));
			}
			}
		});
		btnValider_1.setForeground(Color.MAGENTA);
		btnValider_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnValider_1.setBounds(234, 373, 121, 27);
		this.getContentPane().add(btnValider_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(10, 62, 559, 10);
		this.getContentPane().add(panel);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(258, 4, 325, 52);
		this.getContentPane().add(lblBienvenuSurNotre);
		
		JLabel lblSaisirUnOprateur = new JLabel("saisir un opérateur");
		lblSaisirUnOprateur.setForeground(Color.MAGENTA);
		lblSaisirUnOprateur.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblSaisirUnOprateur.setBackground(Color.WHITE);
		lblSaisirUnOprateur.setBounds(191, 93, 193, 21);
		this.getContentPane().add(lblSaisirUnOprateur);
		
		Id = new JTextField();
		Id.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Id.setColumns(10);
		Id.setBounds(206, 135, 306, 27);
		getContentPane().add(Id);
		
		Nom = new JTextField();
		Nom.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Nom.setColumns(10);
		Nom.setBounds(206, 191, 306, 27);
		getContentPane().add(Nom);
		
		Prenom = new JTextField();
		Prenom.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Prenom.setColumns(10);
		Prenom.setBounds(206, 249, 306, 27);
		getContentPane().add(Prenom);
		
		Numero = new JTextField();
		Numero.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Numero.setColumns(10);
		Numero.setBounds(206, 305, 306, 27);
		getContentPane().add(Numero);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(27, 374, 121, 27);
		getContentPane().add(btnValider_1_1);
	}
	void initForms(){
		Id.setText("");
		Nom.setText("");
		Prenom.setText("");
		Numero.setText("");
		operateur = true;
	}}


