package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JTextField;

import dao.ClientDAO;
import model.Client;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interface_affectation extends JFrame{

	private JFrame frame;
	private JTextField Nom;
	private JTextField Prenom;
	private JTextField IdOperateur;
	private JTextField Numero;
	private JTextField NomEnt;
	private JTextField AdresseEnt;
	private boolean affectation;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_affectation window = new Interface_affectation();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_affectation() {
		getContentPane().setBackground(SystemColor.inactiveCaption);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//frame = new JFrame();
		this.setBounds(100, 100, 591, 586);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 30));
		lblGmao.setBackground(Color.CYAN);
		lblGmao.setBounds(27, 6, 166, 42);
		getContentPane().add(lblGmao);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(10, 58, 559, 10);
		getContentPane().add(panel);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(258, 0, 325, 52);
		getContentPane().add(lblBienvenuSurNotre);
		
		JLabel lblSaisirUnOprateur = new JLabel("Affecter un opérateur");
		lblSaisirUnOprateur.setForeground(Color.MAGENTA);
		lblSaisirUnOprateur.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblSaisirUnOprateur.setBackground(Color.WHITE);
		lblSaisirUnOprateur.setBounds(191, 89, 225, 21);
		getContentPane().add(lblSaisirUnOprateur);
		
		JLabel lblNom = new JLabel("Nom\r\n");
		lblNom.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNom.setBounds(61, 143, 89, 21);
		getContentPane().add(lblNom);
		
		JLabel lblPrnom = new JLabel("Prénom");
		lblPrnom.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblPrnom.setBounds(61, 194, 89, 21);
		getContentPane().add(lblPrnom);
		
		JLabel lblNumerotlphone = new JLabel("Numero_Téléphone");
		lblNumerotlphone.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNumerotlphone.setBounds(47, 306, 166, 21);
		getContentPane().add(lblNumerotlphone);
		
		JLabel lblIdoprateur = new JLabel("id_Opérateur\r\n");
		lblIdoprateur.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblIdoprateur.setBounds(49, 252, 140, 21);
		getContentPane().add(lblIdoprateur);
		
		JLabel lblNomentreprise = new JLabel("Nom_Entreprise");
		lblNomentreprise.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNomentreprise.setBounds(47, 362, 144, 21);
		getContentPane().add(lblNomentreprise);
		
		JLabel lblAdresseentreprise = new JLabel("Adresse_Entreprise");
		lblAdresseentreprise.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblAdresseentreprise.setBounds(49, 418, 166, 21);
		getContentPane().add(lblAdresseentreprise);
		
		JButton btnValider_1 = new JButton("Valider");
		btnValider_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					if(!affectation) {
						Nom.setText("");
						Prenom.setText("");
						IdOperateur.setText("");
						Numero.setText("");
						NomEnt.setText("");
						AdresseEnt.setText("");
						affectation = true;
					}else {
						new AffectationDAO().add(new Affectation(Integer.parseInt(Nom.getText()), Prenom.getText(),IdOperateur.getText(), Numero.getText(), NomEnt.getText(), AdresseEnt.getText()));
						IdOperateur.setEditable(false);
						affectation = false;
						//tree.setModel(new DefaultTreeModel(findNextTreeNode("Stocks")));
				}
			
				}});
		btnValider_1.setForeground(Color.MAGENTA);
		btnValider_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnValider_1.setBounds(225, 489, 121, 27);
		getContentPane().add(btnValider_1);
		
		Nom = new JTextField();
		Nom.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Nom.setColumns(10);
		Nom.setBounds(226, 137, 306, 27);
		getContentPane().add(Nom);
		
		Prenom = new JTextField();
		Prenom.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Prenom.setColumns(10);
		Prenom.setBounds(226, 188, 306, 27);
		getContentPane().add(Prenom);
		
		IdOperateur = new JTextField();
		IdOperateur.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		IdOperateur.setColumns(10);
		IdOperateur.setBounds(228, 246, 306, 27);
		getContentPane().add(IdOperateur);
		
		Numero = new JTextField();
		Numero.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		Numero.setColumns(10);
		Numero.setBounds(228, 300, 306, 27);
		getContentPane().add(Numero);
		
		NomEnt = new JTextField();
		NomEnt.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		NomEnt.setColumns(10);
		NomEnt.setBounds(228, 356, 306, 27);
		getContentPane().add(NomEnt);
		
		AdresseEnt = new JTextField();
		AdresseEnt.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		AdresseEnt.setColumns(10);
		AdresseEnt.setBounds(226, 412, 306, 27);
		getContentPane().add(AdresseEnt);
		
		JButton btnValider_1_1 = new JButton("Retour");
		btnValider_1_1.setForeground(Color.RED);
		btnValider_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnValider_1_1.setBounds(27, 489, 121, 27);
		getContentPane().add(btnValider_1_1);
	}
	void initForms()
	{
		Nom.setText("");
		Prenom.setText("");
		IdOperateur.setText("");
		Numero.setText("");
		NomEnt.setText("");
		AdresseEnt.setText("");
		affectation = true;
	}
}
