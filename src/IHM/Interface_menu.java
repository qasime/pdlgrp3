package IHM;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.TextField;
import java.awt.Button;
import java.awt.Panel;
import javax.swing.JPanel;

public class Interface_menu extends JFrame {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_menu window = new Interface_menu();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_menu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.CYAN);
		frame.getContentPane().setForeground(Color.GREEN);
		frame.setBounds(100, 100, 877, 619);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblGmao = new JLabel("GMAO");
		lblGmao.setBackground(Color.CYAN);
		lblGmao.setForeground(Color.MAGENTA);
		lblGmao.setFont(new Font("Vineta BT", Font.PLAIN, 36));
		lblGmao.setBounds(30, 5, 209, 52);
		frame.getContentPane().add(lblGmao);
		
		
		JLabel lblNewLabel_6 = new JLabel("\r\n");
		lblNewLabel_6.setIcon(new ImageIcon(Interface_menu.class.getResource("/Images/icon1.png")));
		lblNewLabel_6.setBounds(51, 219, 140, 126);
		frame.getContentPane().add(lblNewLabel_6);
		
		JLabel lblNewLabel_8 = new JLabel("");
		
		lblNewLabel_8.setIcon(new ImageIcon(Interface_menu.class.getResource("/Images/Maintenance-icon.png")));
		lblNewLabel_8.setBounds(475, 226, 149, 108);
		frame.getContentPane().add(lblNewLabel_8);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Interface_menu.class.getResource("/Images/user.png")));
		lblNewLabel.setBounds(263, 208, 126, 137);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblBienvenuSurNotre = new JLabel("Bienvenue sur notre logiciel");
		lblBienvenuSurNotre.setForeground(Color.BLACK);
		lblBienvenuSurNotre.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblBienvenuSurNotre.setBackground(Color.CYAN);
		lblBienvenuSurNotre.setBounds(406, 5, 345, 52);
		frame.getContentPane().add(lblBienvenuSurNotre);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(0, 67, 863, 19);
		frame.getContentPane().add(panel);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(216, 5, 84, 65);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setBounds(216, 0, 84, 65);
		frame.getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_9 = new JLabel("");
		lblNewLabel_9.setIcon(new ImageIcon(Interface_menu.class.getResource("/Images/aq.PNG")));
		lblNewLabel_9.setBounds(680, 231, 140, 114);
		frame.getContentPane().add(lblNewLabel_9);
		
		JButton btnGestionResponssable = new JButton("Gestion \r\n");
		btnGestionResponssable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				menu_gestion windows = new menu_gestion();
				windows.setVisible(true);
			}
		});
		btnGestionResponssable.setForeground(Color.RED);
		btnGestionResponssable.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnGestionResponssable.setBackground(Color.ORANGE);
		btnGestionResponssable.setBounds(51, 372, 137, 33);
		frame.getContentPane().add(btnGestionResponssable);
		
		JButton btnUtilisateur = new JButton("Utilisateur");
		btnUtilisateur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				menu_utilisateur windows = new menu_utilisateur();
				windows.setVisible(true);
			}
		});
		btnUtilisateur.setForeground(Color.RED);
		btnUtilisateur.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnUtilisateur.setBackground(Color.ORANGE);
		btnUtilisateur.setBounds(270, 372, 137, 33);
		frame.getContentPane().add(btnUtilisateur);
		
		JButton btnMaintenance = new JButton("Maintenance");
		btnMaintenance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				menu_maintenance windows = new menu_maintenance();
				windows.setVisible(true);
			}
		});
		btnMaintenance.setForeground(Color.RED);
		btnMaintenance.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnMaintenance.setBackground(Color.ORANGE);
		btnMaintenance.setBounds(472, 372, 137, 33);
		frame.getContentPane().add(btnMaintenance);
		
		JButton btnContrat = new JButton("Contrat");
		btnContrat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				Interface_Contrat windows = new Interface_Contrat();
				windows.setVisible(true);
			}
		});
		btnContrat.setForeground(Color.RED);
		btnContrat.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnContrat.setBackground(Color.ORANGE);
		btnContrat.setBounds(680, 372, 137, 33);
		frame.getContentPane().add(btnContrat);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setBounds(0, 500, 863, 72);
		frame.getContentPane().add(panel_1);
		
		JLabel lblGestionDesMaintenances = new JLabel("Gestion des Maintenances Assistées par Ordinateur");
		lblGestionDesMaintenances.setForeground(Color.MAGENTA);
		lblGestionDesMaintenances.setFont(new Font("Vivaldi", Font.BOLD | Font.ITALIC, 32));
		lblGestionDesMaintenances.setBackground(Color.CYAN);
		lblGestionDesMaintenances.setBounds(133, 80, 593, 52);
		frame.getContentPane().add(lblGestionDesMaintenances);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.LIGHT_GRAY);
		panel_2.setBounds(0, 128, 863, 19);
		frame.getContentPane().add(panel_2);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}	
		
	}

	

